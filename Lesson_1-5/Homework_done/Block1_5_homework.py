residence_limit = 90  # 45, 60
schengen_constraint = 180


def date_difference(last, first):
    return last - first + 1


def get_days_for_visits(visits):
    days_for_visits = []
    for visit in visits:
        days_for_visit = 0
        for past_visit in visits:
            if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
                days_for_visit += date_difference(past_visit[1], past_visit[0])
        days_for_visit += date_difference(visit[1], visit[0])
        days_for_visits.append(days_for_visit)
    return days_for_visits


def print_problematic_visits(visits):
    days_for_visits = get_days_for_visits(visits)

    for visit, total_days in zip(visits, days_for_visits):
        if __name__ == '__main__':
            if total_days > residence_limit:
                overstay_time = total_days - residence_limit
                print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')


def print_available_days_for_next_date(visits, next_visit):
    visits_with_next = visits + [[next_visit, next_visit]]
    days_for_visits = get_days_for_visits(visits_with_next)
    days_for_next_visit = residence_limit - days_for_visits[len(days_for_visits) - 1] + 1

    print('Вы сможете пробыть %s дней, если въедете на %s день' % (days_for_next_visit, next_visit))

visits = []

# чтение списка визитов из файла

while True:
    print('v - добавить визит')
    print('p - предсказать визит')
    print('d - удалить визит')
    print('e - выход')
    # проверки на проблемные записи
    print('список визитов: %s' % visits)
    action = input()
    if action == 'v':
        print('Начало:')
        start = int(input())
        print('Конец:')
        end = int(input())
        visits.append([start, end])
        print_problematic_visits(visits)
    elif action == 'd':
        print('Начало:')
        start = int(input())
        print('Конец:')
        end = int(input())
        visits.remove([start, end])
    elif action == 'p':
        print('День въезда:')
        day = int(input())
        print_available_days_for_next_date(visits, day)
    elif action == 'e':
        # запись в файл
        break